#ifndef __TYPE__UNET_H__
#define __TYPE__UNET_H__

#include <stdint.h>

typedef uint8_t umac_t[6];
typedef uint8_t uip4_t[4];
typedef uint16_t uip6_t[8];

#endif /* __TYPE__UNET_H__ */
