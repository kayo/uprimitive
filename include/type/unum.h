#ifndef __UNUM_H__
#define __UNUM_H__

#include <stdint.h>

#ifndef UNUM_BITS
#define UNUM_BITS 32
#endif

#if UNUM_BITS == 8
typedef int8_t unum_sint_t;
typedef uint8_t unum_uint_t;
typedef int8_t unum_sfix_t;
typedef uint8_t unum_ufix_t;
#elif UNUM_BITS == 16
typedef int16_t unum_sint_t;
typedef uint16_t unum_uint_t;
typedef int16_t unum_sfix_t;
typedef uint16_t unum_ufix_t;
#elif UNUM_BITS == 32
typedef int32_t unum_sint_t;
typedef uint32_t unum_uint_t;
typedef int32_t unum_sfix_t;
typedef uint32_t unum_ufix_t;
typedef float unum_real_t;
#elif UNUM_BITS == 64
typedef int64_t unum_sint_t;
typedef uint64_t unum_uint_t;
typedef int64_t unum_sfix_t;
typedef uint64_t unum_ufix_t;
typedef double unum_real_t;
#endif

#endif /* __UNUM_H__ */
