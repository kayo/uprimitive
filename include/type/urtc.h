#ifndef __TYPE__URTC_H__
#define __TYPE__URTC_H__

#include <stdint.h>
#include <stdbool.h>

/**
 * @brief The time type
 */
typedef struct {
  /**
   * @brief The hour [0..23]
   */
  union { uint8_t hour, H, HH, h; };

  /**
   * @brief The minute [0..59]
   */
  union { uint8_t min, minute, M, MM, m; };

  /**
   * @brief The second [0..59]
   */
  union { uint8_t sec, second, S, SS, s; };
} utime_t;

/**
 * @brief Make time constant
 */
#define utime_make(hour, min, sec) { .h = hour, .m = min, .s = sec }

/**
 * @brief Time less than
 */
bool utime_lt(const utime_t *a, const utime_t *b); /* (a < b) */

/**
 * @brief Time greater than
 */
#define utime_gt(a, b) utime_lt(b, a) /* (a > b) <=> (b < a) */

/**
 * @brief Time less or equal than
 */
#define utime_le(a, b) (!utime_lt(b, a)) /* (a <= b) <=> !(b < a) */

/**
 * @brief Time greater or equal than
 */
#define utime_ge(a, b) (!utime_lt(a, b)) /* (a >= b) <=> !(a < b) */

/**
 * @brief Time equals
 */
#define utime_eq(a, b) (!utime_lt(a, b) && !utime_lt(b, a)) /* (a == b) <=> !(a < b) && !(b < a) */

/**
 * @brief Time in range
 */
bool utime_in(const utime_t *t, const utime_t *a, const utime_t *b);

/**
 * @brief Check time is valid
 */
bool utime_valid(const utime_t *t);

/**
 * @brief The date type
 */
typedef struct {
  /**
   * @brief The year
   */
  union { int16_t year, YYYY, y; };

  /**
   * @brief The month of year [1..12]
   */
  union { uint8_t mon, month, mm, m; };

  /**
   * @brief The day of month [1..31]
   */
  union { uint8_t day, date, dd, d; };
} udate_t;

/**
 * @brief Make date constant
 */
#define udate_make(year, mon, date) { .y = year, .m = mon, .d = date }

/**
 * @brief Date less than
 */
bool udate_lt(const udate_t *a, const udate_t *b); /* (a < b) */

/**
 * @brief Date greater than
 */
#define udate_gt(a, b) udate_lt(b, a) /* (a > b) <=> (b < a) */

/**
 * @brief Date less or equal than
 */
#define udate_le(a, b) (!udate_lt(b, a)) /* (a <= b) <=> !(b < a) */

/**
 * @brief Date greater or equal than
 */
#define udate_ge(a, b) (!udate_lt(a, b)) /* (a >= b) <=> !(a < b) */

/**
 * @brief Date equals
 */
#define udate_eq(a, b) (!udate_lt(a, b) && !udate_lt(b, a)) /* (a == b) <=> !(a < b) && !(b < a) */

/**
 * @brief Date in range
 */
bool udate_in(const udate_t *d, const udate_t *a, const udate_t *b);

/**
 * @brief Calculate the day of week [1..7]
 */
uint8_t udate_week_day(const udate_t *d);

/**
 * @brief Check the leap year
 */
bool udate_leap_year(const udate_t *d);

/**
 * @brief Calculate the number of days in month
 */
uint8_t udate_mon_days(const udate_t *d);

/**
 * @brief Check date is valid
 */
bool udate_valid(const udate_t *d);

/**
 * @brief The date and time type
 */
typedef struct {
  udate_t date;
  utime_t time;
} urtc_t;

/**
 * @brief Make date and time constant
 */
#define urtc_make(year, mon, date, hour, min, sec) {  \
    .date = udate_make(year, mon, date),              \
    .time = utime_make(hour, min, sec),               \
  }

/**
 * @brief Date and time less than
 */
bool urtc_lt(const urtc_t *a, const urtc_t *b); /* (a < b) */

/**
 * @brief Date and time greater than
 */
#define urtc_gt(a, b) urtc_lt(b, a) /* (a > b) <=> (b < a) */

/**
 * @brief Date and time less or equal than
 */
#define urtc_le(a, b) (!urtc_lt(b, a)) /* (a <= b) <=> !(b < a) */

/**
 * @brief Date and time greater or equal than
 */
#define urtc_ge(a, b) (!urtc_lt(a, b)) /* (a >= b) <=> !(a < b) */

/**
 * @brief Date and time equals
 */
#define urtc_eq(a, b) (!urtc_lt(a, b) && !urtc_lt(b, a)) /* (a == b) <=> !(a < b) && !(b < a) */

/**
 * @brief Date and time in range
 */
bool urtc_in(const urtc_t *r, const urtc_t *a, const urtc_t *b);

/**
 * @brief Check date and time is valid
 */
#define urtc_valid(r) (utime_valid((r)->time) && udate_valid((r)->date))

enum {
  utime_sec  = 1 << 0,
  utime_min  = 1 << 1,
  utime_hour = 1 << 2,

  udate_date = 1 << 5,
  udate_mon  = 1 << 6,
  udate_year = 1 << 7,

  urtc_time = utime_sec | utime_min | utime_hour,
  urtc_date = udate_date | udate_mon | udate_year,
};

typedef uint8_t urtc_bits_t;

enum {
  umon_jan = 1,
  umon_feb = 2,
  umon_mar = 3,
  umon_apr = 4,
  umon_may = 5,
  umon_jun = 6,
  umon_jul = 7,
  umon_aug = 8,
  umon_sep = 9,
  umon_oct = 10,
  umon_nov = 11,
  umon_dec = 12,
};

enum {
  uday_mon = 1,
  uday_tue = 2,
  uday_wed = 3,
  uday_thu = 4,
  uday_fri = 5,
  uday_sat = 6,
  uday_sun = 7,
};

#endif /* __TYPE__URTC_H__ */
