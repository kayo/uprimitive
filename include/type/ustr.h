#ifndef __USTR_H__
#define __USTR_H__

#include <stddef.h>
#include <string.h>

#include <type/unum.h>

#ifndef ULEN_BITS
#define ULEN_BITS 16
#endif

#if ULEN_BITS == 8
typedef uint8_t ulen_t;
#elif ULEN_BITS == 16
typedef uint16_t ulen_t;
#elif ULEN_BITS == 32
typedef uint32_t ulen_t;
#endif

/**
 * @brief The string type
 */
typedef struct ustr ustr_t;

/**
 * @brief The string resize function
 */
typedef int ustr_resize_t(ustr_t *str, ulen_t len);

/**
 * @brief The string struct
 */
struct ustr {
  /**
   * @brief The pointer to (de)allocator
   */
  ustr_resize_t *res;
  /**
   * @brief The starting position of string
   *
   * This position is used as current point for reading string data
   */
  ulen_t pos;
  /**
   * @brief The end position of string
   *
   * This position is used as current point for writinging string data
   */
  ulen_t len;
  /**
   * @brief The pointer to string data buffer
   */
  char *ptr;
}; /* 12 bytes */

/**
 * @brief Get pointer to beginning of string
 */
#define ustr_str(str) ((str)->ptr)

/**
 * @brief Get position in string
 */
#define ustr_pos(str) ((str)->pos)

/**
 * @brief Get length of string
 */
#define ustr_len(str) ((str)->len)

/**
 * @brief Get pointer to beginning of string
 */
#define ustr_beg(str) (ustr_str(str) + ustr_pos(str))

/**
 * @brief Get pointer to end of string
 */
#define ustr_end(str) (ustr_str(str) + ustr_len(str))

/**
 * @brief Get the pointer in string
 */
#define ustr_ptr(str, idx) (ustr_str(str) + (ulen_t)(idx))

/**
 * @brief Initialize string using external buffer
 */
void ustr_extern_init(ustr_t *str, char *ptr);

/**
 * @brief Initialize string using heap memory
 */
void ustr_heap_init(ustr_t *str, char *ptr);

/**
 * @brief The static buffer for string
 */
typedef struct {
  ulen_t len;
  char ptr[0];
} ustr_static_t;

/**
 * @brief Define static buffer for string
 */
#define ustr_static(name, size)   \
  struct {                        \
    ulen_t len;                   \
    char ptr[size];               \
  } name = {                      \
    size,                         \
    ""                            \
  }

/**
 * @brief Initialize string using static buffer
 */
#define ustr_static_init(str, buf) \
  ustr_static_init_(str, (ustr_static_t*)(buf))

void ustr_static_init_(ustr_t *str, ustr_static_t *buf);

/**
 * @brief Release string data
 */
static inline void ustr_release(ustr_t *str) {
  str->res(str, 0);
}

/**
 * @brief Resize string
 */
static inline int ustr_resize(ustr_t *str, ulen_t len) {
  return str->res(str, len);
}

/**
 * @brief Extend string
 */
static inline int ustr_extend(ustr_t *str, ulen_t len) {
  return ustr_resize(str, ustr_len(str) + len);
}

/**
 * @brief Shrink string
 */
static inline int ustr_shrink(ustr_t *str, ulen_t len) {
  return ustr_resize(str, ustr_len(str) - len);
}

/**
 * @brief Terminate string with null character in C-style
 */
static inline int ustr_term(ustr_t *str) {
  if (0 != ustr_extend(str, 1)) {
    return -1;
  }
  
  str->ptr[str->len++] = '\0';
  
  return 1;
}

/**
 * @brief Cut string to length
 */
static inline void ustr_cut(ustr_t *str, ulen_t len) {
  str->len = len;
}

/**
 * @brief Get binary data from beginning of string
 */
int ustr_getbd(ustr_t *str, void *ptr, ulen_t len);

/**
 * @brief Put binary data to end of string
 */
int ustr_putbd(ustr_t *str, const void *ptr, ulen_t len);

/**
 * @brief Get generic value from beginning of string as raw binary data
 */
#define ustr_getvr(str, val)                    \
  (ustr_getbd(str, &val, sizeof(val)) ==        \
   sizeof(val) ? (int)sizeof(val) : -1)

/**
 * @brief Put generic value to end of string as raw binary data
 */
#define ustr_putvr(str, val)                    \
  (ustr_putbd(str, &val, sizeof(val)) ==        \
   sizeof(val) ? (int)sizeof(val) : -1)

/**
 * @brief Get length-prefixed C-string from beginning of string
 */
int ustr_getsl(ustr_t *str, char *ptr, ulen_t len);

/**
 * @brief Put length-prefixed C-string to end of string
 */
int ustr_putsl(ustr_t *str, const char *ptr, ulen_t len);

/**
 * @brief Get length-prefixed C-string from beginning of string
 */
static inline int ustr_getsl0(ustr_t *str, char *ptr, ulen_t len) {
  int res = ustr_getsl(str, ptr, len - 1);

  if (res < 0) {
    return res;
  }

  ptr[res] = '\0';

  return res;
}

/**
 * @brief Put length-prefixed C-string to end of string
 */
static inline int ustr_putsl0(ustr_t *str, const char *ptr) {
  return ustr_putsl(str, ptr, strlen(ptr));
}

/**
 * @brief Get C-string from beginning of string
 */
static inline int ustr_getsn(ustr_t *str, char *ptr, ulen_t len) {
  return ustr_getbd(str, ptr, len);
}

/**
 * @brief Put C-string to end of string
 */
static inline int ustr_putsn(ustr_t *str, const char *ptr, ulen_t len) {
  return ustr_putbd(str, ptr, len);
}

/**
 * @brief Put string to end of string
 */
#define ustr_putus(str, ustr)                     \
  ustr_putsn(str, ustr_str(ustr), ustr_len(ustr))

/**
 * @brief Put constant C-string to end of string
 */
#define ustr_putsc(str, cstr)                   \
  ustr_putsn(str, cstr, sizeof(cstr)-1)

/**
 * @brief Put null-terminated C-string to end of string without processing
 */
static inline int ustr_putsr(ustr_t *str, const char *nstr) {
  return ustr_putsn(str, nstr, strlen(nstr));
}

/**
 * @brief Put C-string to end of string with escaping
 */
int ustr_putse(ustr_t *str, const char *ptr, ulen_t len);

/**
 * @brief Put null-terminated C-string to end of string with escaping
 */
static inline int ustr_putsq(ustr_t *str, const char *nstr) {
  return ustr_putse(str, nstr, strlen(nstr));
}

/**
 * @brief Put string data as hexadecimal string
 */
int ustr_putsx(ustr_t *str, const char *ptr, ulen_t len);

/**
 * @brief Put single character to end of string
 */
int ustr_putc(ustr_t *str, char chr);

/**
 * @brief Put unsigned int as varint to end of string
 */
int ustr_putuv(ustr_t *str, unum_uint_t val);

/**
 * @brief Get varint as unsigned int from string
 */
int ustr_getuv(ustr_t *str, unum_uint_t *val);

/**
 * @brief Put unsigned int as decimal number to end of string
 */
int ustr_putud(ustr_t *str, unum_uint_t val);

/**
 * @brief Put unsigned int as hexadecimal number to end of string
 */
int ustr_putux(ustr_t *str, unum_uint_t val);

/**
 * @brief Put unsigned int as octal number to end of string
 */
int ustr_putuo(ustr_t *str, unum_uint_t val);

/**
 * @brief Put signed int as decimal number to end of string
 */
int ustr_putsd(ustr_t *str, unum_sint_t val);

/**
 * @brief Put unsigned fixed point number to end of string with last zeros
 */
int ustr_putufz(ustr_t *str, unum_ufix_t val, uint8_t fix, uint8_t exp);

/**
 * @brief Put signed fixed point number to end of string with last zeros
 */
int ustr_putsfz(ustr_t *str, unum_sfix_t val, uint8_t fix, uint8_t exp);

/**
 * @brief Put unsigned fixed point number to end of string skipping last zeros
 */
int ustr_putuf(ustr_t *str, unum_ufix_t val, uint8_t fix, uint8_t exp);

/**
 * @brief Put signed fixed point number to end of string skipping last zeros
 */
int ustr_putsf(ustr_t *str, unum_sfix_t val, uint8_t fix, uint8_t exp);


#if UNUM_BITS >= 32
/**
 * @brief Put real number to end of string
 */
int ustr_putrp(ustr_t *str, unum_real_t val, uint8_t exp);
#endif

#endif /*__USTR_H__*/
