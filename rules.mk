primitive.BASEPATH := $(subst $(dir $(abspath $(CURDIR)/xyz)),,$(dir $(abspath $(lastword $(MAKEFILE_LIST)))))

TARGET.LIBS += libprimitive
libprimitive.INHERIT ?= firmware
libprimitive.CDIRS := $(primitive.BASEPATH)include
libprimitive.SRCS := $(patsubst %,$(primitive.BASEPATH)src/%.c,ustr urtc)
