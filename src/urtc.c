#include <type/urtc.h>

bool utime_lt(const utime_t *a, const utime_t *b) {
  return a->hour < b->hour || (a->hour == b->hour && (a->min < b->min || (a->min == b->min && a->sec < b->sec)));
}

bool utime_in(const utime_t *t, const utime_t *a, const utime_t *b) {
  bool from_a = utime_ge(t, a);
  bool to_b = utime_lt(t, b);
  return utime_lt(a, b) ? from_a && to_b : from_a || to_b;
}

bool utime_valid(const utime_t *t) {
  return t->hour < 24 && t->minute < 60 && t-> second < 60;
}

bool udate_lt(const udate_t *a, const udate_t *b) {
  return a->year < b->year || (a->year == b->year && (a->mon < b->mon || (a->mon == b->mon && a->date < b->date)));
}

bool udate_in(const udate_t *d, const udate_t *a, const udate_t *b) {
  bool from_a = udate_ge(d, a);
  bool to_b = udate_lt(d, b);
  return udate_lt(a, b) ? from_a && to_b : from_a || to_b;
}

uint8_t udate_week_day(const udate_t *date) {
  uint16_t y = date->y;
  uint16_t m = date->m;
  uint16_t d = date->d;
  
  d += m < 3 ? y-- : y - 2;
  
  return (23 * m / 9 + d + 4 + y / 4 - y / 100 + y / 400 ) % 7;
}

bool udate_leap_year(const udate_t *d) {
  return d->year % 4 ? false : d->year % 100 ? true : d->year % 400 ? false : true;
}

uint8_t udate_mon_days(const udate_t *d) {
  switch (d->mon) {
  case 1: return 31;
  case 2: return udate_leap_year(d) ? 29 : 28;
  case 3: return 31;
  case 4: return 30;
  case 5: return 31;
  case 6: return 30;
  case 7: return 31;
  case 8: return 31;
  case 9: return 31;
  case 10: return 31;
  case 11: return 30;
  case 12: return 31;
  default: return 0;
  }
}

bool udate_valid(const udate_t *d) {
  return d->mon >= 1 && d->mon <= 12 && d->date >= 1 && d->date <= udate_mon_days(d);
}

bool urtc_lt(const urtc_t *a, const urtc_t *b) {
  return udate_lt(&a->date, &b->date) | (udate_le(&a->date, &b->date) & utime_lt(&a->time, &b->time));
}

bool urtc_in(const urtc_t *r, const urtc_t *a, const urtc_t *b) {
  bool from_a = urtc_ge(r, a);
  bool to_b = urtc_lt(r, b);
  return urtc_lt(a, b) ? from_a && to_b : from_a || to_b;
}
