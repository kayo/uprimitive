#include <type/ustr.h>

/* string in heap memory */

#include <malloc.h>
#include <stdlib.h>

#define i2h(h) ((h) < 10 ? (h) + '0' : (h) + ('a' - 10))

#define likely(x) __builtin_expect((x), 1)
#define unlikely(x) __builtin_expect((x), 0)

static int ustr_extern_resize(ustr_t *str, ulen_t len) {
  (void)str;
  (void)len;
  
  return 0;
}

void ustr_extern_init(ustr_t *str, char *ptr) {
  str->res = ustr_extern_resize;
  ustr_pos(str) = 0;
  ustr_len(str) = 0;
  ustr_str(str) = ptr;
}

#define roundup32(x) (--(x), (x)|=(x)>>1, (x)|=(x)>>2, (x)|=(x)>>4, (x)|=(x)>>8, (x)|=(x)>>16, ++(x))

static int ustr_heap_resize(ustr_t *str, ulen_t len) {
  if (len > (ustr_str(str) ? malloc_usable_size(ustr_str(str)) : 0)) {
    roundup32(len);
    char *tmp = (char*)realloc(ustr_str(str), len);
    
    if (tmp != NULL) {
      ustr_str(str) = tmp;
    } else {
      return -1;
    }
  } else if (len == 0) {
    ustr_str(str) = realloc(ustr_str(str), len);
    ustr_len(str) = 0;
  }
  
  return 0;
}

void ustr_heap_init(ustr_t *str, char *ptr) {
  str->res = ustr_heap_resize;
  ustr_str(str) = ptr;
  ustr_pos(str) = 0;
  ustr_len(str) = ptr == NULL ? 0 : strlen(ptr);
}

#define containerof(ptr, type, member) ((type*)((char*)(ptr) - offsetof(type, member)))

/* string in static memory */

static int ustr_static_resize(ustr_t *str, ulen_t len) {
  ustr_static_t *buf = containerof(ustr_str(str), ustr_static_t, ptr);
  
  if (len > buf->len) {
    return -1;
  } else if (len == 0) {
    ustr_len(str) = 0;
  }
  
  return 0;
}

void ustr_static_init_(ustr_t *str, ustr_static_t *buf) {
  str->res = ustr_static_resize;
  ustr_pos(str) = 0;
  ustr_len(str) = 0;
  ustr_str(str) = buf->ptr;
}

/* binary data functions */

int ustr_getbd(ustr_t *str, void *ptr, ulen_t len) {
  ulen_t has_len = ustr_len(str) - ustr_pos(str);
  
  if (len > has_len) {
    len = has_len;
  }

  memcpy(ptr, ustr_beg(str), len);

  ustr_pos(str) += len;

  return len;
}

int ustr_putbd(ustr_t *str, const void *ptr, ulen_t len) {
  ulen_t new_len = ustr_len(str) + len;
  
  if (0 > ustr_resize(str, new_len)) {
    return -1;
  }
  
  memcpy(ustr_end(str), ptr, len);
  
  ustr_len(str) = new_len;
  
  return len;
}

/* string builder functions */

int ustr_getsl(ustr_t *str, char *ptr, ulen_t len) {
  unum_uint_t cnt;
  int res;
  
  res = ustr_getuv(str, &cnt);
  
  if (res < 0) {
    return res;
  }

  if (cnt < len) {
    /* cutoff the string */
    len = cnt;
  }
  
  res = ustr_getsn(str, ptr, len);

  if (res < 0) {
    return res;
  }

  cnt -= len;

  /* skip the end of string */
  if (ustr_len(str) - ustr_pos(str) < (ulen_t)cnt) {
    return -1;
  }
  
  ustr_pos(str) += cnt;

  return len;
}

int ustr_putsl(ustr_t *str, const char *ptr, ulen_t len) {
  ulen_t out;
  int res = ustr_putuv(str, len);

  if (res < 0) {
    return res;
  }

  out = res;

  res = ustr_putsn(str, ptr, len);

  if (res < 0) {
    return res;
  }

  return out + res;
}

int ustr_putsx(ustr_t *str, const char *ptr, ulen_t len) {
	len <<= 1;
	ulen_t new_len = ustr_len(str) + len;
	
	if (0 > ustr_resize(str, new_len)) {
    return -1;
  }
  
	char *dst = ustr_end(str);
	char *end = ustr_str(str) + new_len;
	
  for (; dst < end; ) {
    *dst++ = i2h((*ptr >> 4) & 0x0f);
    *dst++ = i2h(*ptr++ & 0x0f);
  }

	ustr_len(str) = new_len;
	
	return len;
}

int ustr_putse(ustr_t *str, const char *ptr, ulen_t len) {
	/* prepare */
  const char *c = ptr;
	const char *e = ptr + len;
	
	len = 0;
	
	for (; c < e; c++) {
		if (unlikely(*c == '\0' ||
					(*c >= '\a' && *c <= '\r') ||
					*c == '\\' ||
					*c == '"' ||
					*c == '\'')) {
			len += 2;
		} else {
			len ++;
		}
  }

	ulen_t new_len = ustr_len(str) + len;
	
	if (0 > ustr_resize(str, new_len)) {
    return -1;
  }
	
  /* put string data */
	c = ptr;
	char *d = ustr_end(str);

	static const char r[] = "abtnvfr";
	
  for (; c < e; c++) {
    if (unlikely(*c == '\0')) {
      *d++ = '\\';
			*d++ = '0';
      continue;
    } else if (unlikely(*c >= '\a' && *c <= '\r')) {
      *d++ = '\\';
			*d++ = r[*c - '\a'];
      continue;
    } else if (unlikely(*c == '"' ||
                        *c == '\'' ||
                        *c == '\\')) {
      *d++ = '\\';
    }
    *d++ = *c;
  }
  
	ustr_len(str) = new_len;
	
  return len;
}

int ustr_putc(ustr_t *str, char chr) {
  return ustr_putsn(str, &chr, 1);
}

int ustr_putuv(ustr_t *str, unum_uint_t val) {
  uint8_t bytes = 1;
  unum_uint_t _val = val;
  
  for (; _val >= (1 << 7); _val >>= 7, bytes ++);
  
  if (0 > ustr_extend(str, bytes)) {
    return -1;
  }
  
  ulen_t pos = ustr_len(str);
  ulen_t end = pos + bytes - 1;
  
  for (; pos < end; val >>= 7, pos ++) {
    *ustr_ptr(str, pos) = (val & 0x7f) | (1 << 7);
  }
  
  *ustr_ptr(str, pos) = val & 0x7f;
  
  ustr_len(str) += bytes;
  
  return bytes;
}

int ustr_getuv(ustr_t *str, unum_uint_t *val) {
  uint8_t bytes = 1;

  {
    ulen_t pos = ustr_pos(str);
    
    for (; *ustr_ptr(str, pos) & (1 << 7); pos ++, bytes ++) {
      if (pos >= ustr_len(str)) {
        return -1;
      }
    }
    
    /*if (bytes * 7 >= UNUM_BITS) {
      return -1;
    }*/
  }

  {
    uint8_t shift = 0;
    
    *val = 0;
    
    for (; ; ustr_pos(str) ++, shift += 7) {
      *val |= ((unum_uint_t)(*ustr_beg(str) & 0x7f)) << shift;
      
      if (!(*ustr_beg(str) & (1 << 7))) {
        ustr_pos(str) ++;
        break;
      }
    }
  }

  return bytes;
}

/* fast estimation count of decimal digits in numeric value */
static uint8_t ustr_digud(unum_uint_t val) {
#define r(res) return res;
#define c(exp, lt, ge) if (val < ((unum_uint_t)1e##exp)) { lt } else { ge }
#if UNUM_BITS >= 8
#define c8   \
  c(1,       \
    r(1),    \
    c(2,     \
      r(2),  \
      r(3)))
#endif
#if UNUM_BITS >= 16
#define c16     \
  c(2,          \
    c(1,        \
      r(1),     \
      r(2)),    \
    c(3,        \
      r(3),     \
      c(4,      \
        r(4),   \
        r(5))))
#endif
#if UNUM_BITS >= 32
#define c32         \
  c(5,              \
    c16,            \
    c(7,            \
      c(6,          \
        r(6),       \
        r(7)),      \
      c(8,          \
        r(8),       \
        c(9,        \
          r(9),     \
          r(10)))))
#endif
#if UNUM_BITS >= 64
#define c64             \
  c(10,                 \
    c32,                \
    c(15,               \
      c(12,             \
        c(11,           \
          r(11),        \
          r(12)),       \
        c(13,           \
          r(13),        \
          c(14,         \
            r(14),      \
            r(15)))),   \
      c(17,             \
        c(16,           \
          r(16),        \
          r(17)),       \
        c(18,           \
          r(18),        \
          c(19,         \
            r(19),      \
            r(20))))))
#endif
  
#if UNUM_BITS == 8
  c8;
#elif UNUM_BITS == 16
  c16;
#elif UNUM_BITS == 32
  c32;
#elif UNUM_BITS == 64
  c64;
#endif

#ifdef c8
#undef c8
#endif
#ifdef c16
#undef c16
#endif
#ifdef c32
#undef c32
#endif
#ifdef c64
#undef c64
#endif
#undef r
#undef c
}

static uint8_t ustr_digux(unum_uint_t val) {
  uint8_t dig = 0;
  
  for (; val > 0; dig++, val >>= 4);
  
  return dig > 0 ? dig : 1;
}

static uint8_t ustr_diguo(unum_uint_t val) {
  uint8_t dig = 0;
  
  for (; val > 0; dig++, val >>= 3);
  
  return dig > 0 ? dig : 1;
}

/*
put number digits:
dig - number of digits
mod - number module (2, 8, 10, 16, etc.)
*/
#define putdig(str, val, dig, mod) {            \
    char *b = ustr_end(str);                    \
    char *p = b + dig - 1;                      \
                                                \
    goto r1;                                    \
  r0:                                           \
    val /= mod;                                 \
  r1:                                           \
    *p-- = i2h(val % mod);                      \
                                                \
    if (p >= b) {                               \
      if (val > 0) {                            \
        goto r0;                                \
      }                                         \
    } else {                                    \
      goto r3;                                  \
    }                                           \
  r2:                                           \
    *p-- = '0';                                 \
                                                \
    if (p >= b) {                               \
      goto r2;                                  \
    }                                           \
  r3:                                           \
    ustr_len(str) += dig;                       \
  }

/*
put number digits:
dig - number of digits
mod - number module (2, 8, 10, 16, etc.)
slz - skip last zeros
*/
#define putdigslz(str, val, dig, mod) {         \
    char *b = ustr_end(str);                    \
    char *p = b + dig - 1;                      \
    uint8_t v;                                  \
    uint8_t d = 0;                              \
                                                \
    goto r1;                                    \
  r0:                                           \
    val /= mod;                                 \
  r1:                                           \
    v = val % mod;                              \
                                                \
    if (v && !d) {                              \
      d = 1;                                    \
    }                                           \
                                                \
    if (!d) {                                   \
      p--;                                      \
      dig--;                                    \
      goto r0;                                  \
    }                                           \
                                                \
    *p-- = i2h(v);                              \
                                                \
    if (p >= b) {                               \
      if (val > 0) {                            \
        goto r0;                                \
      }                                         \
    } else {                                    \
      goto r3;                                  \
    }                                           \
  r2:                                           \
    *p-- = '0';                                 \
                                                \
    if (p >= b) {                               \
      goto r2;                                  \
    }                                           \
  r3:                                           \
    ustr_len(str) += dig;                       \
  }

static void _ustr_putud(ustr_t *str, unum_uint_t val, uint8_t dig) {
  putdig(str, val, dig, 10);
}

int ustr_putud(ustr_t *str, unum_uint_t val) {
  uint8_t dig = ustr_digud(val);
  
  if (0 > ustr_extend(str, dig)) {
    return -1;
  }
  
  _ustr_putud(str, val, dig);
  
  return dig;
}

int ustr_putux(ustr_t *str, unum_uint_t val) {
  uint8_t dig = ustr_digux(val);
  
  if (0 > ustr_extend(str, dig)) {
    return -1;
  }
  
  putdig(str, val, dig, 16);
  
  return dig;
}

int ustr_putuo(ustr_t *str, unum_uint_t val) {
  uint8_t dig = ustr_diguo(val);
  
  if (0 > ustr_extend(str, dig)) {
    return -1;
  }
  
  putdig(str, val, dig, 8);
  
  return dig;
}

int ustr_putsd(ustr_t *str, unum_sint_t val) {
  uint8_t neg = 0;
  
  if (val < 0) {
    val = -val;
    neg = 1;
  }
  
  uint8_t dig = ustr_digud(val);
  
  if (0 > ustr_extend(str, dig + neg)) {
    return -1;
  }
  
  if (neg) {
    *ustr_end(str) = '-';
    ustr_len(str) ++;
  }
  
  _ustr_putud(str, val, dig);
  
  return dig + neg;
}

/* fast get 10^n */
static const unum_uint_t _ustr_decexp_val[] = {
  1,        /* 0 */
  10,       /* 1 */
  100,      /* 2 */
  1000,     /* 3 */
  10000,    /* 4 */
  100000,   /* 5 */
};

#define _ustr_decexp_max (sizeof(_ustr_decexp_val) / sizeof(_ustr_decexp_val[0]) - 1)

/* fast calc 10^n */
static unum_uint_t ustr_decexp(uint8_t exp) {
  unum_uint_t dec = 1;
  for (; exp > _ustr_decexp_max; ) {
    dec *= _ustr_decexp_val[_ustr_decexp_max];
    exp -= _ustr_decexp_max;
  }
  return dec * _ustr_decexp_val[exp];
}

int ustr_putufz(ustr_t *str, unum_ufix_t val, uint8_t fix, uint8_t exp) {
  unum_uint_t dec = ustr_decexp(exp);
  unum_uint_t ipart = val >> fix;
  unum_uint_t fpart = ((val & (((unum_ufix_t)1 << fix) - 1)) * dec) >> (fix - 1);
  uint8_t lsb = fpart & 0x1;
  fpart >>= 1;

  if (lsb) {
    fpart ++;
    if (fpart >= dec) {
      fpart -= dec;
      ipart ++;
    }
  }

  int res = ustr_putsd(str, ipart);

  if (res < 0 || exp == 0) {
    return res;
  }

  if (0 > ustr_extend(str, exp + 1)) {
    return -1;
  }

  *ustr_end(str) = '.';
  ustr_len(str) ++;

  putdig(str, fpart, exp, 10);

  return res + 1 + exp;
}

int ustr_putsfz(ustr_t *str, unum_sfix_t val, uint8_t fix, uint8_t exp){
  uint8_t neg = 0;

  if (val < 0) {
    if (0 > ustr_extend(str, 2)) {
      return -1;
    }

    *ustr_end(str) = '-';
    ustr_len(str) ++;

    val = -val;
    neg = 1;
  }

  return neg + ustr_putufz(str, val, fix, exp);
}

int ustr_putuf(ustr_t *str, unum_ufix_t val, uint8_t fix, uint8_t exp) {
  unum_uint_t dec = ustr_decexp(exp);
  unum_uint_t ipart = val >> fix;
  unum_uint_t fpart = ((val & (((unum_ufix_t)1 << fix) - 1)) * dec) >> (fix - 1);
  uint8_t lsb = fpart & 0x1;
  fpart >>= 1;

  if (lsb) {
    fpart ++;
    if (fpart >= dec) {
      fpart -= dec;
      ipart ++;
    }
  }

  int res = ustr_putsd(str, ipart);

  if (res < 0 || exp == 0 || fpart == 0) {
    return res;
  }

  if (0 > ustr_extend(str, exp + 1)) {
    return -1;
  }

  *ustr_end(str) = '.';
  ustr_len(str) ++;

  putdigslz(str, fpart, exp, 10);

  return res + 1 + exp;
}

int ustr_putsf(ustr_t *str, unum_sfix_t val, uint8_t fix, uint8_t exp){
  uint8_t neg = 0;
  
  if (val < 0) {
    if (0 > ustr_extend(str, 2)) {
      return -1;
    }
    
    *ustr_end(str) = '-';
    ustr_len(str) ++;
    
    val = -val;
    neg = 1;
  }
  
  return neg + ustr_putuf(str, val, fix, exp);
}

#if UNUM_BITS >= 32
static unum_sint_t f2i(unum_real_t val, uint8_t exp){
  for (; exp > 0; exp--) val *= 10;
  
  unum_real_t afp = val - (unum_sint_t)val;
  
  return val + (afp < -0.5 ? -1 : afp < 0.5 ? 0 : 1);
}

int ustr_putrp(ustr_t *str, unum_real_t val, uint8_t exp){
  uint8_t neg = 0;
  
  if (val < 0) {
    if (0 > ustr_extend(str, 2)) {
      return -1;
    }

    *ustr_end(str) = '-';
    ustr_len(str) ++;
    
    val = -val;
    neg = 1;
  }
  
  unum_uint_t ipart = val;
  unum_uint_t fpart = f2i(val - ipart, exp);
  
  for (; fpart != 0 && (fpart % 10) == 0; fpart /= 10, exp --);
  
  int res = ustr_putud(str, ipart);
  
  if (res < 0 || fpart < 1 || exp < 1) {
    return res;
  }
  
  if (0 > ustr_extend(str, exp + 1)) {
    return -1;
  }
  
  *ustr_end(str) = '.';
  ustr_len(str) ++;
  
  _ustr_putud(str, fpart, exp);
  
  return neg + res + exp + 1;
}
#endif /* UNUM_BITS >= 32 */
