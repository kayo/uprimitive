#include "test.h"

#include <type/urtc.h>

#define test_cmp(type, a, op, b, exp) {      \
    info(#type "_" #op "(" #a ", " #b ") "); \
    const type##_t _a = type##_make a;       \
    const type##_t _b = type##_make b;       \
    bool act = type##_##op(&_a, &_b);        \
    if (exp == act) {                        \
      pass();                                \
    } else {                                 \
      fail("Result mismatch! Expected: %s "  \
           "Actual: %s",                     \
           exp ? "true" : "false",           \
           act ? "true" : "false");          \
    }                                        \
  }

#define test_in(type, v, a, b, exp) {        \
    info(#type "_in(" #v ", " #a ", " #b     \
         ") ");                              \
    const type##_t _v = type##_make v;       \
    const type##_t _a = type##_make a;       \
    const type##_t _b = type##_make b;       \
    bool act = type##_in(&_v, &_a, &_b);     \
    if (exp == act) {                        \
      pass();                                \
    } else {                                 \
      fail("Result mismatch! Expected: %s "  \
           "Actual: %s",                     \
           exp ? "true" : "false",           \
           act ? "true" : "false");          \
    }                                        \
  }

#define test_week_day(date, exp) {           \
    info("udate_week_day(" #date ") ");      \
    const udate_t _date = udate_make date;   \
    uint8_t act = udate_week_day(&_date);    \
    if (exp == act) {                        \
      pass();                                \
    } else {                                 \
      fail("Result mismatch! Expected: %u "  \
           "Actual: %u", exp, act);          \
    }                                        \
  }

#define test_leap_year(year, exp) {          \
    info("udate_leap_year(" #year ") ");     \
    const udate_t _date = udate_make(year,   \
                                     1, 1);  \
    bool act = udate_leap_year(&_date);      \
    if (exp == act) {                        \
      pass();                                \
    } else {                                 \
      fail("Result mismatch! Expected: %u "  \
           "Actual: %u", exp, act);          \
    }                                        \
  }

#define test_mon_days(year, mon, exp) {      \
    info("udate_mon_days(" #year ", " #mon   \
         ") ");                              \
    const udate_t _date = udate_make(year,   \
                                     mon,    \
                                     1);     \
    uint8_t act = udate_mon_days(&_date);    \
    if (exp == act) {                        \
      pass();                                \
    } else {                                 \
      fail("Result mismatch! Expected: %u "  \
           "Actual: %u", exp, act);          \
    }                                        \
  }

int main() {
  /* time */
  test_cmp(utime, (11, 22, 33), eq, (11, 22, 33), true);
  test_cmp(utime, (11, 22, 33), lt, (11, 22, 33), false);
  test_cmp(utime, (11, 22, 33), le, (11, 22, 33), true);
  test_cmp(utime, (11, 22, 33), gt, (11, 22, 33), false);
  test_cmp(utime, (11, 22, 33), ge, (11, 22, 33), true);

  test_cmp(utime, (10, 22, 33), lt, (11, 22, 33), true);
  test_cmp(utime, (11, 21, 33), lt, (11, 22, 33), true);
  test_cmp(utime, (11, 22, 32), lt, (11, 22, 33), true);

  test_cmp(utime, (10, 22, 33), le, (11, 22, 33), true);
  test_cmp(utime, (11, 21, 33), le, (11, 22, 33), true);
  test_cmp(utime, (11, 22, 32), le, (11, 22, 33), true);

  test_cmp(utime, (10, 22, 33), gt, (11, 22, 33), false);
  test_cmp(utime, (11, 21, 33), gt, (11, 22, 33), false);
  test_cmp(utime, (11, 22, 32), gt, (11, 22, 33), false);

  test_cmp(utime, (10, 22, 33), ge, (11, 22, 33), false);
  test_cmp(utime, (11, 21, 33), ge, (11, 22, 33), false);
  test_cmp(utime, (11, 22, 32), ge, (11, 22, 33), false);

  test_in(utime, (12, 20, 30), (12, 01, 30), (13, 01, 01), true);
  test_in(utime, (12, 20, 30), (13, 01, 01), (12, 01, 30), false);

  /* date */
  test_cmp(udate, (11, 22, 33), eq, (11, 22, 33), true);
  test_cmp(udate, (11, 22, 33), lt, (11, 22, 33), false);
  test_cmp(udate, (11, 22, 33), le, (11, 22, 33), true);
  test_cmp(udate, (11, 22, 33), gt, (11, 22, 33), false);
  test_cmp(udate, (11, 22, 33), ge, (11, 22, 33), true);

  test_cmp(udate, (10, 22, 33), lt, (11, 22, 33), true);
  test_cmp(udate, (11, 21, 33), lt, (11, 22, 33), true);
  test_cmp(udate, (11, 22, 32), lt, (11, 22, 33), true);

  test_cmp(udate, (10, 22, 33), le, (11, 22, 33), true);
  test_cmp(udate, (11, 21, 33), le, (11, 22, 33), true);
  test_cmp(udate, (11, 22, 32), le, (11, 22, 33), true);

  test_cmp(udate, (10, 22, 33), gt, (11, 22, 33), false);
  test_cmp(udate, (11, 21, 33), gt, (11, 22, 33), false);
  test_cmp(udate, (11, 22, 32), gt, (11, 22, 33), false);

  test_cmp(udate, (10, 22, 33), ge, (11, 22, 33), false);
  test_cmp(udate, (11, 21, 33), ge, (11, 22, 33), false);
  test_cmp(udate, (11, 22, 32), ge, (11, 22, 33), false);

  test_in(udate, (12, 20, 30), (12, 01, 30), (13, 01, 01), true);
  test_in(udate, (12, 20, 30), (13, 01, 01), (12, 01, 30), false);

  test_week_day((1985, 12, 24), 2);
  test_week_day((1986,  1,  8), 3);
  test_week_day((1991,  9, 23), 1);
  test_week_day((2000,  1,  1), 6);
  test_week_day((2018, 10, 11), 4);

  test_leap_year(2012, true);
  test_leap_year(2013, false);
  test_leap_year(2014, false);
  test_leap_year(2015, false);
  test_leap_year(2016, true);
  test_leap_year(2018, false);

  test_mon_days(2012, 2, 29);
  test_mon_days(2012, 1, 31);
  test_mon_days(2018, 9, 31);
  test_mon_days(2018, 11, 30);
}
