#include "test.h"

#include <type/ustr.h>
#include <inttypes.h>

#if UNUM_BITS >= 64
#define unum_uint_fmt PRIu64
#elif UNUM_BITS >= 32
#define unum_uint_fmt PRIu32
#elif UNUM_BITS >= 16
#define unum_uint_fmt PRIu16
#elif UNUM_BITS >= 8
#define unum_uint_fmt PRIu8
#endif

#define test_vr(type, val)                      \
  {                                             \
    info("ustr_putvr " #type " " #val);         \
    ustr_t ustr;                                \
    ustr_extern_init(&ustr, buf);               \
    type src = val;                             \
    int res = ustr_putvr(&ustr, src);           \
    if (res != sizeof(type)) {                  \
      fail("Invalid result value! "             \
           "Expected: %d Actual: %d",           \
           (int)sizeof(type), res);             \
    } else {                                    \
      pass();                                   \
      info("ustr_getvr " #type " " #val);       \
      type dst;                                 \
      int res = ustr_getvr(&ustr, dst);         \
      if (res != sizeof(type)) {                \
        fail("Invalid result value! "           \
             "Expected: %d Actual: %d",         \
             (int)sizeof(type), res);           \
      } else if (src != dst) {                  \
        fail("Values not equals!");             \
      } else {                                  \
        pass();                                 \
      }                                         \
    }                                           \
  }

#define test_xtos(tag, val, str)                                  \
  {                                                               \
    info("ustr_put" #tag " " str);                                \
    ustr_t ustr;                                                  \
    ustr_extern_init(&ustr, buf);                                 \
    ustr_put##tag(&ustr, val);                                    \
    ustr_term(&ustr);                                             \
    if (strcmp(str, ustr_str(&ustr)) != 0) {                      \
      fail("Invalid result value! Expected: '%s' Actual: '%s'",   \
           str, ustr_str(&ustr));                                 \
    } else {                                                      \
      pass();                                                     \
    }                                                             \
    ustr_release(&ustr);                                          \
  }

#define test_ftos(tag, exp, val, str)                             \
  {                                                               \
    info("ustr_put" #tag " " str);                                \
    ustr_t ustr;                                                  \
    ustr_extern_init(&ustr, buf);                                 \
    ustr_put##tag(&ustr,                                          \
                  ((double)(val)) *                               \
                  ((unum_uint_t)1 << (UNUM_BITS/2)),              \
                  UNUM_BITS/2, exp);                              \
    ustr_term(&ustr);                                             \
    if (strcmp(str, ustr_str(&ustr)) != 0) {                      \
      fail("Invalid result value! Expected: '%s' Actual: '%s'",   \
           str, ustr_str(&ustr));                                 \
    } else {                                                      \
      pass();                                                     \
    }                                                             \
    ustr_release(&ustr);                                          \
  }

#define test_rtos(exp, val, str)                                  \
  {                                                               \
    info("ustr_putrp " str);                                      \
    ustr_t ustr;                                                  \
    ustr_extern_init(&ustr, buf);                                 \
    ustr_putrp(&ustr, val, exp);                                  \
    ustr_term(&ustr);                                             \
    if (strcmp(str, ustr_str(&ustr)) != 0) {                      \
      fail("Invalid result value! Expected: '%s' Actual: '%s'",   \
           str, ustr_str(&ustr));                                 \
    } else {                                                      \
      pass();                                                     \
    }                                                             \
    ustr_release(&ustr);                                          \
  }

#define test_uv(val, len)                                         \
  {                                                               \
    info("ustr_(put|get)uv " #val);                               \
    ustr_t ustr;                                                  \
    ustr_extern_init(&ustr, buf);                                 \
    int r = ustr_putuv(&ustr, val);                               \
    if (r != len) {                                               \
      fail("The put length mismatch! Expected: %u Actual: %u",    \
           len, r);                                               \
    } else {                                                      \
      unum_uint_t rval;                                           \
      r = ustr_getuv(&ustr, &rval);                               \
      if (r != len) {                                             \
        fail("The get length mismatch! Expected: %u Actual: %u",  \
             len, r);                                             \
      } else if ((unum_uint_t)(val) != rval) {                    \
        fail("The value mismatch! Expected: %" unum_uint_fmt      \
             " Actual: %" unum_uint_fmt,                          \
             (unum_uint_t)val, rval);                             \
      } else {                                                    \
        pass();                                                   \
      }                                                           \
    }                                                             \
    ustr_release(&ustr);                                          \
  }

#define test_sl(val, len, rlen)                                   \
  {                                                               \
    info("ustr_(put|get)sl " val);                                \
    ustr_t ustr;                                                  \
    ustr_extern_init(&ustr, buf);                                 \
    int r = ustr_putsl0(&ustr, val);                              \
    if (r != len) {                                               \
      fail("The put length mismatch! Expected: %d Actual: %d",    \
           (int)len, r);                                          \
    } else {                                                      \
      char str[rlen];                                             \
      r = ustr_getsl0(&ustr, str, rlen);                          \
      if (r != (rlen < sizeof(val) ? rlen: sizeof(val)) - 1) {    \
        fail("The get length mismatch! Expected: %d Actual: %d",  \
             (int)(rlen < sizeof(val) ? rlen: sizeof(val)) - 1,   \
             r);                                                  \
      } else {                                                    \
        if (0 != memcmp(val, str, r)) {                           \
          fail("The string mismatch! Expected: %s Acrual: %s",    \
               val, str);                                         \
        } else {                                                  \
          pass();                                                 \
        }                                                         \
      }                                                           \
    }                                                             \
  }

int main() {
  char buf[256];

  test_vr(uint8_t, 0x55);
  test_vr(int8_t, 0xaa);
#if UNUM_BITS >= 16
  test_vr(int16_t, 0x505a);
  test_vr(uint16_t, 0xa505);
#endif
#if UNUM_BITS >= 32
  test_vr(int32_t, 0x5500aa00);
  test_vr(uint32_t, 0x00aa0055);
#endif
#if UNUM_BITS >= 64
  test_vr(int64_t, 0x5500550000aa00aa);
  test_vr(uint64_t, 0x00aa00aa55005500);
#endif

  test_uv(0, 1);
  test_uv(1, 1);
  test_uv(120, 1);
  test_uv(127, 1);
  test_uv(128, 2);
  test_uv(129, 2);
  test_uv(250, 2);
  test_uv(255, 2);
#if UNUM_BITS >= 16
  test_uv(16383, 2);
  test_uv(16384, 3);
  test_uv(16385, 3);
  test_uv(16535, 3);
#endif
#if UNUM_BITS >= 32
  test_uv(2097151, 3);
  test_uv(2097152, 4);
  test_uv(4294967295, 5);
#endif
#if UNUM_BITS >= 64
  test_uv(4294967296, 5);
  test_uv(34359738367, 5);
  test_uv(34359738368, 6);
  test_uv(34359738369, 6);
  test_uv(9223372036854775808u, 10);
  test_uv(10000000000000000000u, 10);
#endif
  
  test_xtos(ud, 0, "0");
  test_xtos(ud, 1, "1");
  test_xtos(ud, 13, "13");
  test_xtos(ud, 10, "10");
  test_xtos(ud, 100, "100");
  test_xtos(ud, 110, "110");
  test_xtos(ud, 200, "200");
  test_xtos(ud, 123, "123");
  test_xtos(ud, 250, "250");
  test_xtos(ud, 251, "251");
  test_xtos(ud, 100u, "100");
#if UNUM_BITS >= 16
  test_xtos(ud, 2015, "2015");
  test_xtos(ud, 1000u, "1000");
  test_xtos(ud, 10000u, "10000");
#endif
#if UNUM_BITS >= 32
  test_xtos(ud, 12032105, "12032105");
  test_xtos(ud, 100000u, "100000");
  test_xtos(ud, 1000000u, "1000000");
  test_xtos(ud, 10000000u, "10000000");
  test_xtos(ud, 100000000u, "100000000");
  test_xtos(ud, 1000000000u, "1000000000");
#endif
#if UNUM_BITS >= 64
  test_xtos(ud, 10000000000u, "10000000000");
  test_xtos(ud, 100000000000u, "100000000000");
  test_xtos(ud, 1000000000000u, "1000000000000");
  test_xtos(ud, 10000000000000u, "10000000000000");
  test_xtos(ud, 100000000000000u, "100000000000000");
  test_xtos(ud, 1000000000000000u, "1000000000000000");
  test_xtos(ud, 10000000000000000u, "10000000000000000");
  test_xtos(ud, 100000000000000000u, "100000000000000000");
  test_xtos(ud, 1000000000000000000u, "1000000000000000000");
  test_xtos(ud, 10000000000000000000u, "10000000000000000000");
#endif
  
  test_xtos(sd, 0, "0");
  test_xtos(sd, 1, "1");
  test_xtos(sd, -1, "-1");
  test_xtos(sd, 13, "13");
  test_xtos(sd, 10, "10");
  test_xtos(sd, 100, "100");
  test_xtos(sd, -10, "-10");
  test_xtos(sd, -100, "-100");
  test_xtos(sd, 123, "123");
  test_xtos(sd, -123, "-123");
#if UNUM_BITS >= 16
  test_xtos(sd, 2015, "2015");
  test_xtos(sd, -2015, "-2015");
#endif
#if UNUM_BITS >= 32
  test_xtos(sd, 12032105, "12032105");
  test_xtos(sd, -2301051, "-2301051");
#endif

  test_xtos(ux, 0, "0");
  test_xtos(ux, 1, "1");
  test_xtos(ux, 10, "a");
  test_xtos(ux, 15, "f");
  test_xtos(ux, 16, "10");
  test_xtos(ux, 18, "12");
  test_xtos(ux, 29, "1d");
#if UNUM_BITS >= 16
  test_xtos(ux, 0x10e, "10e");
  test_xtos(ux, 0xf230, "f230");
#endif
#if UNUM_BITS >= 32
  test_xtos(ux, 0x10000, "10000");
  test_xtos(ux, 0x100000, "100000");
  test_xtos(ux, 0x1000000, "1000000");
  test_xtos(ux, 0x10000000, "10000000");
#endif
#if UNUM_BITS >= 64
  test_xtos(ux, 0x100000000, "100000000");
  test_xtos(ux, 0x1000000000, "1000000000");
  test_xtos(ux, 0x10000000000, "10000000000");
  test_xtos(ux, 0x100000000000, "100000000000");
  test_xtos(ux, 0x1000000000000, "1000000000000");
  test_xtos(ux, 0x10000000000000, "10000000000000");
  test_xtos(ux, 0x100000000000000, "100000000000000");
  test_xtos(ux, 0x1000000000000000, "1000000000000000");
#endif

  test_xtos(uo, 0, "0");
  test_xtos(uo, 1, "1");
  test_xtos(uo, 10, "12");
  test_xtos(uo, 15, "17");
  test_xtos(uo, 16, "20");
  test_xtos(uo, 18, "22");
  test_xtos(uo, 29, "35");
#if UNUM_BITS >= 16
  test_xtos(uo, 0416, "416");
  test_xtos(uo, 0171060, "171060");
#endif
#if UNUM_BITS >= 32
  test_xtos(uo, 01000000, "1000000");
  test_xtos(uo, 010000000, "10000000");
  test_xtos(uo, 0100000000, "100000000");
  test_xtos(uo, 01000000000, "1000000000");
  test_xtos(uo, 010000000000, "10000000000");
#endif
#if UNUM_BITS >= 64
  test_xtos(uo, 0100000000000, "100000000000");
  test_xtos(uo, 01000000000000, "1000000000000");
  test_xtos(uo, 010000000000000, "10000000000000");
  test_xtos(uo, 0100000000000000, "100000000000000");
  test_xtos(uo, 01000000000000000, "1000000000000000");
  test_xtos(uo, 010000000000000000, "10000000000000000");
  test_xtos(uo, 0100000000000000000, "100000000000000000");
  test_xtos(uo, 01000000000000000000, "1000000000000000000");
  test_xtos(uo, 010000000000000000000, "10000000000000000000");
  test_xtos(uo, 0100000000000000000000, "100000000000000000000");
  test_xtos(uo, 01000000000000000000000, "1000000000000000000000");
#endif

  test_ftos(uf, 10, 0, "0");
  test_ftos(uf, 10, 1, "1");
  test_ftos(uf, 10, 12, "12");
  test_ftos(uf, 1, 0.5, "0.5");
#if UNUM_BITS >= 16
  test_ftos(uf, 10, 234, "234");
  test_ftos(uf, 2, 1.01, "1.01");
#endif
#if UNUM_BITS >= 32
  test_ftos(uf, 10, 1234, "1234");
  test_ftos(uf, 4, 0.0001, "0.0001");
  test_ftos(uf, 4, 1000.0001, "1000.0001");
#endif
#if UNUM_BITS >= 64
  test_ftos(uf, 10, 12345678, "12345678");
  test_ftos(uf, 8, 0.00000001, "0.00000001");
  test_ftos(uf, 8, 1000.00000001, "1000.00000001");
#endif

  test_ftos(sf, 10, 0, "0");
  test_ftos(sf, 10, 1, "1");
  test_ftos(sf, 10, -1, "-1");
  test_ftos(sf, 10, 7, "7");
  test_ftos(sf, 10, -7, "-7");
  test_ftos(sf, 1, 0.5, "0.5");
  test_ftos(sf, 1, -0.5, "-0.5");
#if UNUM_BITS >= 16
  test_ftos(sf, 10, 123, "123");
  test_ftos(sf, 10, -123, "-123");
  test_ftos(sf, 2, 1.01, "1.01");
  test_ftos(sf, 2, -1.01, "-1.01");
#endif
#if UNUM_BITS >= 32
  test_ftos(sf, 10, 1234, "1234");
  test_ftos(sf, 10, -1234, "-1234");
  test_ftos(sf, 4, 0.0001, "0.0001");
  test_ftos(sf, 4, -0.0001, "-0.0001");
  test_ftos(sf, 4, 1000.0001, "1000.0001");
  test_ftos(sf, 4, -1000.0001, "-1000.0001");
#endif
#if UNUM_BITS >= 64
  test_ftos(sf, 10, 12345678, "12345678");
  test_ftos(sf, 10, -12345678, "-12345678");
  test_ftos(sf, 8, 0.00000001, "0.00000001");
  test_ftos(sf, 8, -0.00000001, "-0.00000001");
  test_ftos(sf, 8, 1000.00000001, "1000.00000001");
  test_ftos(sf, 8, -1000.00000001, "-1000.00000001");
#endif

#if UNUM_BITS >= 32
  test_rtos(10, 0, "0");
  test_rtos(10, 1, "1");
  test_rtos(10, -1, "-1");
  test_rtos(10, 1234, "1234");
  test_rtos(10, -1234, "-1234");
  test_rtos(7, 1.001, "1.001");
  test_rtos(7, -1.001, "-1.001");
  test_rtos(10, 0.00001, "0.00001");
  test_rtos(10, -0.00001, "-0.00001");
  test_rtos(4, 1000.0001, "1000.0001");
  test_rtos(4, -1000.0001, "-1000.0001");
#endif

  test_sl("Abracadabra!", 13, 13);
  test_sl("Abracadabra! Abracadabra! Abracadabra! Abracadabra! Abracadabra! Abracadabra! Abracadabra! Abracadabra! Abracadabra! Abracadabra!", 131, 120);
  
  return 0;
}
